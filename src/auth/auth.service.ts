/* eslint-disable */
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/auth.entity';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly jwtService: JwtService
    ){}

    async createUser(usuarioNuevo:CreateUserDto):Promise<User> {
        const nuevo = new User();
        nuevo.id = usuarioNuevo.id;
        nuevo.username = usuarioNuevo.username;
        nuevo.email = usuarioNuevo.email;
        nuevo.password = usuarioNuevo.password;
        return this.userRepository.save(nuevo);
    }

    async login(email: string, password: string): Promise<{ token: string; usuario: string; email: string }> {
        const usuario = await this.userRepository.findOneBy({ email: email });
      
        if (!usuario) {
          throw new Error('El usuario no existe!!');
        }
      
        const contrasenaAlmacenada = usuario.password;
        if (password !== contrasenaAlmacenada) {
          throw new Error('Contraseña incorrecta');
        }
      
        const token = this.generateToken(usuario);
        return { token, usuario: usuario.username, email: usuario.email };
      }
      

    private generateToken(usuario: User): string {
        const payload = { userId: usuario.id, username: usuario.username };
        const token = this.jwtService.sign(payload);
        return token;
      }
    
}
