/* eslint-disable */
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginDto } from './dto/login.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(private authService:AuthService){}
    @Post('register')
  async register(@Body() createUserDto: CreateUserDto, @Res() response) {
    try {
      const user = await this.authService.createUser(createUserDto);
      response.status(HttpStatus.CREATED).json(user);
    } catch (error) {
      response.status(HttpStatus.BAD_REQUEST).json({ mensaje: 'Error en la creación del usuario' });
    }
  }

  @Post('login')
  async login(@Body() loginDto: LoginDto, @Res() response) {
    try {
      const user = await this.authService.login(loginDto.email, loginDto.password);
      response.status(HttpStatus.OK).json(user);
    } catch (error) {
      console.log(error);
      response.status(HttpStatus.UNAUTHORIZED).json({ mensaje: error.message });
    }
  }

}
